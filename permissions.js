function showPermissions () {
  getRequest("https://api.myjson.com/bins/6zc42").then(response => {
    let results = JSON.parse(response); // assume its JSON safe, otherwise should check this in a "IsJSON" utility using try catch

    let common = getCommonPermissions(results);

    let container = document.getElementById("cpus");
    container.innerHTML = '';

    for(let i = 0 ; i < results.length; i++){
      container.innerHTML +=
       `<div>
          <h3>${results[i].cpu}</h3>
          <ul>
            ${results[i].permissions.map( p => {
              if(!(p in common)){ // do not show permissions that are in common array
                return `<li>${p}</li>`
              }
            }).join('')}
          </ul>
        </div>`;
    }

    if(Object.keys(common).length !== 0){
      let commonHTML = ''
      for(let c in common){
        commonHTML += `<li>${c}</li>`;
      }
      container.innerHTML += `<div> <h3>Common Permissions</h3> <ul>${commonHTML}</ul> </div>`
    }

  }).catch(error => {
    document.getElementById("permissions").innerText = error;
  })
}

function getCommonPermissions (cpus) {
  let common = {};
  let temp = {};

  for(p of cpus[0].permissions){ // turning into object for constant time access
    common[p] = true;
  }
  for (let i = 1; i < cpus.length; i++){ // O(c) constant time bcz the number of cpus are limited
    for (let j = 0; j < cpus[i].permissions.length ; j++){ // O(n)
      if (cpus[i].permissions[j] in common){ // O(c)
        temp[cpus[i].permissions[j]] = true;
      }
    }
    common = temp; // update common permissions
    temp = {};
  }
  return common;
}

function getRequest (url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = () => {
      if(xhr.status === 200){
        resolve(xhr.response)
      }
      else{
        reject(xhr.statusText);
      }
    }
    xhr.onerror = () => {
      reject(xhr.statusText);
    }
    xhr.send(null)
  })
}

showPermissions();